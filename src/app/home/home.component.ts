import { Component, OnInit } from '@angular/core'
import { RadSideDrawer } from 'nativescript-ui-sidedrawer'
import { Application, Observable, GestureEventData, Color, TouchGestureEventData, Dialogs, inputType, EventData, View } from '@nativescript/core'

@Component({
  selector: 'Home',
  templateUrl: './home.component.html',
})

export class HomeComponent extends Observable implements OnInit { 
  name:string="";
  personCount="";
  addVis="hidden"; 
  dialogOpen="hidden";
  currentTimeField=null;
  constructor() {
    super()
    console.log("bla1")
  }

  ngOnInit(): void {
    // Init your component properties here.
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>Application.getRootView()
    sideDrawer.showDrawer()
  }

  onTap(args: TouchGestureEventData): void {
    console.log(args.view.backgroundColor)
    console.log(new Color ("green"))
    const bgColor = new Color(args.view.backgroundColor.toString())
    const greenColor = new Color ("green")
    if(bgColor.equals(greenColor)) {
      Dialogs.confirm({title:"Wollen sie die Reservierung wirklich Löschen?", message:"", okButtonText:"Löschen", cancelButtonText:"Abbrechen"}).then(function(result){
        if(result){
         args.view.text=""
         args.view.backgroundColor = new Color ("white")
        }
      })
      
    } else{
      args.view.backgroundColor = new Color ("green")
      this.addVis="visible";
      this.currentTimeField=args.view;
    }

  }

  addReservation(args: EventData): void {
    this.dialogOpen="visible";

  }

  onReservation(args: EventData): void {
    console.log(this.name);
    this.dialogOpen="hidden";
    this.currentTimeField.text=this.name+" | "+ this.personCount;

  }

  


}
